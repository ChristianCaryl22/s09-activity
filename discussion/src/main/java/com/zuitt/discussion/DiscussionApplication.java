package com.zuitt.discussion;

import java.util.ArrayList;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
@RequestMapping("/greeting")
//The "@RestController" annotation tells Spring Boot that this application will function as an endpoint that will be used in handling web request
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}



	@GetMapping("/hello")
	//Maps a get request to the route "/hello" and the method "hello()"
	//localhost:8080/
	public String hello () {
		return "Hello World";
	}


	//Routes with a string query
	//Dynamic data is obtained from the URL's string query
	//"%s" specifies that the value to be included in the format is of any data type
	//http://localhost:8080/hi?name=Christian
	@GetMapping("/hi")

		public String hi(@RequestParam(value = "name", defaultValue = "John") String name) {
		return String.format("Hi %s", name);
		}

	//Multiple Parameters
	//http://localhost:8080/friend?name=Ash&friend=Pikachu

	@GetMapping("/friend")
	public String friend(@RequestParam(value = "name", defaultValue = "Joe") String name, @RequestParam(value = "friend", defaultValue = "Jane") String friend) {
		return String.format("Hello %s! My name is %s", friend, name);
	}


	//Routes with path variables

	@GetMapping("/hello/{name}")
	//localhost:8080/hello/joe
	//@PathVariable annotation allows us to extract data directly from the URL
	public String courses (@PathVariable("name") String name) {
		return String.format ("Nice to meet you %s!", name);
	}

	//Activity 09
	//Initialize a new ArrayList called enrollees in the DiscussionApplication class.
		ArrayList enrollees = new ArrayList<>();

		//Create a /enroll route that will accept a query string with the parameter of user
		@GetMapping("/enroll")
		public String enroll(@RequestParam String user) {
			enrollees.add(user);
			return "Thank you for enrolling, " + user + "!";
		}

		//Create a new /getEnrollees route which will return the content of the enrollees ArrayList as a string.
		@GetMapping("/getEnrollees")
		public ArrayList getEnrollees(){
			return enrollees;
		}

		//Create a /nameage route with that will accept multiple query string parameters of name and age and will
		//return a Hello (name)! My age is (age). message.
		@GetMapping("/nameage")
		public String nameAge(@RequestParam String name, @RequestParam int age) {
			return "Hello " + name + "! My age is " + age + ".";
		}

		@GetMapping("/courses/{id}")
		public String courseId(@PathVariable ("id") String id) {
			String message;
			if(id.equals("java101")){
				message = "Name: Java101, Schedule: MWF 8:00 AM - 11:00 AM, Price: PHP 3000";
			}
			else if(id.equals("sql101")){
				message = "Name: SQL101, Schedule: MWF 12:00 PM - 4:00 PM, Price: PHP 3100";
			}
			else if(id.equals("javaee101")){
				message = "Name: JavaEE101, Schedule: TTH 8:00 AM - 11:00 AM, Price: PHP 3000";
			}
			else{
				message = "Course cannot be found!";
			}

			return message;
		}


}
